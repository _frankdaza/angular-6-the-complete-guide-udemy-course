import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  signinForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.signinForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSignin() {
    if (this.signinForm.valid) {
      const email: string = (this.signinForm.controls.email.value).trim().toLowerCase();
      const password: string = (this.signinForm.controls.password.value).trim().toLowerCase();
      
      try {
        this.authService.signinUser(email, password);        
      } catch (error) {
        console.log('catch console');
        confirm(`Error ${error}`)
      }
      
    }
  }

}
