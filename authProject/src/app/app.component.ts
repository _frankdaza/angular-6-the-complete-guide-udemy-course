import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'authProject';

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyDk82_0uFF46mEgpQITVFsBnq4BDLZ53QY",
      authDomain: "auth-angular-udemy.firebaseapp.com"
    });
  }

}
